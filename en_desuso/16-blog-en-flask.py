import sqlite3
from datetime import datetime

from flask import Flask, render_template, request


app = Flask(__name__)

@app.route('/')
def inicio():
    #valores:
    # id, titulo, resumen, imagen, texto, autor, fecha

    # Obtener los parámetros de la consulta
    pagina = int(request.args.get('pagina', 1))
    items_por_pagina = 2

    # Establecer la conexión a la base de datos
    conn = sqlite3.connect('elibre.db')  # Reemplaza 'nombre_base_datos.db' con el nombre de tu base de datos SQLite
    cursor = conn.cursor()

    # Obtener el total de elementos
    cursor.execute("SELECT COUNT(*) FROM blog")  # Reemplaza 'tabla' con el nombre de tu tabla
    total_items = cursor.fetchone()[0]

    # Calcular el número total de páginas
    total_paginas = (total_items + items_por_pagina - 1) // items_por_pagina

    # Calcular el offset y ejecutar la consulta
    offset = (pagina - 1) * items_por_pagina
    cursor.execute("SELECT id, titulo, resumen, imagen, fecha FROM blog  ORDER BY id DESC LIMIT ? OFFSET ?", (items_por_pagina, offset))  # Reemplaza 'tabla' con el nombre de tu tabla
    items = cursor.fetchall()

    # Cerrar la conexión a la base de datos
    cursor.close()
    conn.close()

    # Renderizar la plantilla y pasar los datos necesarios
    return render_template('inicio.html', items=items, pagina=pagina, total_paginas=total_paginas)

@app.route('/publicacion/<int:valor>')
def buscar_publicaciones(valor):
    # Establecer la conexión a la base de datos
    conn = sqlite3.connect('elibre.db')  # Reemplaza 'nombre_base_datos.db' con el nombre de tu base de datos SQLite
    cursor = conn.cursor()

    # Ejecutar la consulta utilizando el valor entero recibido
    cursor.execute("SELECT * FROM blog WHERE id = ?", (valor,))   # Reemplaza 'tabla' con el nombre de tu tabla y 'columna' con el nombre de la columna en la que deseas buscar

    # Obtener los resultados de la consulta
    publicacion = cursor.fetchall()

    # Cerrar la conexión a la base de datos
    cursor.close()
    conn.close()

    return render_template('publicacion.html', publicacion=publicacion )
    #return f"Resultados: {publicacion}"


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, debug=True)
