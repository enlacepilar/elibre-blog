import sqlite3
from datetime import datetime

from bottle import route, template, request, run


# @route('/static/imagenes')
# def server_static(filepath):
#     return static_file(filepath, root='/')

@route('/')
def inicio():
    # Obtener los parámetros de la consulta
    pagina = int(request.query.get('pagina', 1))
    items_por_pagina = 2

    # Establecer la conexión a la base de datos
    conn = sqlite3.connect('elibre.db')  # Reemplaza 'nombre_base_datos.db' con el nombre de tu base de datos SQLite
    cursor = conn.cursor()

    # Obtener el total de elementos
    cursor.execute("SELECT COUNT(*) FROM blog")  # Reemplaza 'tabla' con el nombre de tu tabla
    total_items = cursor.fetchone()[0]

    # Calcular el número total de páginas
    total_paginas = (total_items + items_por_pagina - 1) // items_por_pagina

    # Calcular el offset y ejecutar la consulta
    offset = (pagina - 1) * items_por_pagina
    cursor.execute("SELECT * FROM blog  ORDER BY id DESC LIMIT ? OFFSET ?", (items_por_pagina, offset))  # Reemplaza 'tabla' con el nombre de tu tabla
    items = cursor.fetchall()

    # Cerrar la conexión a la base de datos
    cursor.close()
    conn.close()

    # Renderizar la plantilla y pasar los datos necesarios
    return template('inicio', items=items, pagina=pagina, total_paginas=total_paginas)


if __name__ == '__main__':
    run(host='localhost', port=8080, debug=True)
