
## Blog de Elibre
Es un blog realizado con Python y Flask (inicialmente con Bottle pero tuve problemas con las imágenes) y almacenando las entradas en SQLite. La idea es poder escribir sin complicaciones en el script 15, darle una imagen si tuviera y subir por FTP todo a un lugar en mi Raspeberry de casa. Este proyecto se bifurca del tedio de:
* No querer tener Wordpress por todo lo que ello implica.
* Probar algo mejor (más sencillo para vagos como yo) que los sitios estáticos de Pelican, en donde vengo publicando hace un tiempo.
* Scriptear, que es lo que más me gusta de la informática.
* Sacar algunas fotos y hacer gifs animados.

## ¿Qué más?

Se puede clonar y adaptar, ¡¡es Software Libre papá!!

Al día de hoy podés hacer lo siguiente:

* Crear publicaciones desde cualquier dispositivo desde la ruta /crear

* Crear una presentación en el sector presentación a través del script "querés-crearte una presentación"

* Subir gif animados

* Subir fotos y si son JPG te las reduce para más placeeeer, diría Homero.

* Si no tenés fotos y querés subir igual algo, reemplazá en la carpeta "/static/imagenes" la foto "sin-imagen.webp" porque te la inserta por defecto :)

* Podés publicar desde el script de python o desde la web, en cuanlquier lado y desde cualquier dispositivo.


## Pasos para arrancar (pasos 'casi' infalibles):

* Entrá a tu servidor.

* Clonate este repo (generalmente va en /var/www/html):  

``` 
 git clone https://gitlab.com/enlacepilar/elibre-blog.git

```

* Dale los derechos correspondientes:

```
    sudo chmod -R 775 elibre-blog/
```

* Entrá a la carpeta que clonaste. Aislá el entorno de Python:

```
python3 -m venv envTuBlog

```

* Una vez hecho eso, ahora sí aislamos el entorno:

```

source envTuBlog\bin\activate

```

Ahora todo lo que instales de python queda queda ahí y no se extiende más allá de esos límites.

* Ahora instalá los requerimientos del archivo TXT:

```

pip install -r requerimientos_despliegue.txt

```

Te va a instalar todo lo que hace falta para desplegar.


* Después de eso Corré el script para crear una entrada (15-bloguero.py) porque te crea la base de datos y la tabla "blog" en sqlite:

```
python 15-bloguero.py 

```

* Corré, si querés y vas a publicar desde la web, el archivo de python del mismo modo "crea-clave-supersecreta.py". Esto te va a permitir publicar con clave desde la ruta /crear desde tu propio sitio web. 

* Corré, si querés hacer una breve presentación "querés-crearte una presentación.py". Esto, va a crear una tabla nueva en la base con el mismo nombre y lo vas a ver reflejado en el sector "presentación", valga la redundancia, no (donde más) :)

* Desplegá Si no sabés como desplegar (el hecho de hacer visible el sitio Flask en internet desde un servidor con Nginx (un gestor de sitios web) podés ver un tutorial (yo lo hice con ese y me andivo, bien, sólo le cambié algunos nombres):

<a href="https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-gunicorn-and-nginx-on-ubuntu-20-04-es" target="_blank">Tuto despliegue flask</a>

Si no querés el tutorial porque sos muy vago como yo, entonces lo conveniente sería tener presente algunas cosas:

* Necesitás un complemento como Gunicorn (pip install gunicorn), que lo que hace es gestionar las peticiones desde Nginx a tu app Flask. 
Para ello tenemos que crear un servicio en Gnu/Linux que le permita arrancar todo el tiempo. Yo lamentablemente uso el goloso Systemd porque no conozco otro, entonces vamos a: 

```
cd /etc/systemd/system
```

y creamos un servicio nuevo llamado *elibre.service*.
Alli ponemos esto apuntando a nuestra app (recordar que las rutas son de ejemplo. Tus rutas son tus rutas:

```
[Unit]  
Description=gunicorn daemon  
After=network.target

[Service]  
User=TU USUARIO  
Group=www-data  
WorkingDirectory=/var/www/html/elibre-blog 
ExecStart=/var/www/html/elibre-blog/envElibre/bin/gunicorn --access-logfile - --workers 3 --bind unix:/var/log/elibre_log/elibre.sock elibreApp:app

[Install]  
WantedBy=multi-user.target
```

A diferencia de lo que pasa con Django, tenés que apuntar a la app directamente y no al wsgi. 
Luego vamos a Nginx.
* Necesitas un gestor de sitios como Nginx para que redirija a Guinicorn

En el gestor /etc/nginx/sites-available ponemos esto, donde el enlace es lo que tenemos en el servicio (vamos a tener que crear la ruta esa en /var/log, directorio y derechos para el usuario y/o grupo:

```
server {
    listen 80;
    server_name your_domain www.your_domain;

    location / {
        include proxy_params;
        proxy_pass http://unix:/var/log/elibre_log/elibre.sock;
    }
}
```
Después guardamos. Luego creamos el enlace simbólico:
```
sudo ln -s /etc/nginx/sites-available/elibre /etc/nginx/sites-enabled
```
Reiniciamos el servicio nginx
y reiniciamos los demonios (servicios):
```
sudo service nginx reload
sudo systemctl daemon-reload
```

¡Y listo! Todo debería andar de las mil maravillas.

