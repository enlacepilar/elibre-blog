import sqlite3
from datetime import datetime

con = sqlite3.connect("elibre.db")
cursor=con.cursor()


def salida():
    print ('')
    sal = input ("carga mas (enter) (X para salir)")

    if sal == "X":
        con.close()
        print ('')
        print ('Cierro conexión')
        print ("Adiosin")
        
    else:
        carga()


############### Función carga entrada ###################  
#valores:  
# id, titulo, resumen, imagen, texto, autor, fecha
   


def carga():
    titulo=input('Titulo de la publicación: \n')
    resumen=input('Breve resumen: \n')
    imagen=input('¿Nombre de la imagen? (si tiene): \n ')
    texto =input('Texto de la publicación: \n ')
    autor = "tajamar"
    fecha=datetime.strftime(datetime.now(), '%d-%m-%Y %H:%M')

    cursor.execute("INSERT INTO blog(titulo,resumen,imagen,texto,autor,fecha) VALUES (?,?,?,?,?,?)",(titulo,resumen,imagen,texto,autor,fecha))
    con.commit()
    print ("datos cargados")   
   

    ### imprime los datos cargados
    datos2 = cursor.execute(" SELECT * FROM blog WHERE ID = (SELECT MAX(ID) FROM blog);")
    for i in datos2:
        print (i)
    salida()
############### Fin de función carga ###################


def crear_tabla():
    cursor.execute( """CREATE TABLE blog
            (id INTEGER PRIMARY KEY AUTOINCREMENT,
            titulo VARCHAR (255),
            resumen VARCHAR (255),
            imagen VARCHAR (255),
            autor VARCHAR (255),
            texto TEXT,
            fecha DATETIME
            );
    """)
    carga()


# Verifica si la tabla existe
print('Verificamos si la tabla existe en la base:')
verifica = cursor.execute(
  """SELECT name FROM sqlite_master WHERE type='table' AND name='blog';""").fetchall()
 
if verifica == []:
    print('No se encontró la tabla. La creamos...')
    crear_tabla()
else:
    print('¡Tabla blog encontrada! Continuamos.')
    carga()
