import sqlite3
from datetime import datetime

from flask import Flask, render_template, request, flash, redirect, url_for

from werkzeug.exceptions import HTTPException
from werkzeug.utils import secure_filename
from flask.debughelpers import DebugFilesKeyError

from PIL import Image

import os


app = Flask(__name__)
app.secret_key = 'probá esto'  # Reemplaza con tu propia clave secreta

#region ESTE ES EL SECTOR DEL INICIO
@app.route('/')
def inicio():
    #valores:
    # id, titulo, resumen, imagen, texto, autor, fecha

    # Obtener los parámetros de la consulta
    pagina = int(request.args.get('pagina', 1))
    items_por_pagina = 5

    # Establecer la conexión a la base de datos
    conn = sqlite3.connect('elibre.db')  # Reemplaza 'nombre_base_datos.db' con el nombre de tu base de datos SQLite
    cursor = conn.cursor()

    # Obtener el total de elementos
    cursor.execute("SELECT COUNT(*) FROM blog")  # Reemplaza 'tabla' con el nombre de tu tabla
    total_items = cursor.fetchone()[0]

    # Calcular el número total de páginas
    total_paginas = (total_items + items_por_pagina - 1) // items_por_pagina

    # Calcular el offset y ejecutar la consulta
    offset = (pagina - 1) * items_por_pagina
    cursor.execute("SELECT id, titulo, resumen, imagen, fecha FROM blog  ORDER BY id DESC LIMIT ? OFFSET ?", (items_por_pagina, offset))  # Reemplaza 'tabla' con el nombre de tu tabla
    items = cursor.fetchall()

    # Cerrar la conexión a la base de datos
    cursor.close()
    conn.close()

    # Renderizar la plantilla y pasar los datos necesarios
    return render_template('inicio.html', items=items, pagina=pagina, total_paginas=total_paginas)

@app.route('/publicacion/<int:valor>')
def buscar_publicaciones(valor):
    # Establecer la conexión a la base de datos
    conn = sqlite3.connect('elibre.db')  # Reemplaza 'nombre_base_datos.db' con el nombre de tu base de datos SQLite
    cursor = conn.cursor()

    # Ejecutar la consulta utilizando el valor entero recibido
    cursor.execute("SELECT * FROM blog WHERE id = ?", (valor,))   # Reemplaza 'tabla' con el nombre de tu tabla y 'columna' con el nombre de la columna en la que deseas buscar

    # Obtener los resultados de la consulta
    publicacion = cursor.fetchall()

    # Cerrar la conexión a la base de datos
    cursor.close()
    conn.close()

    return render_template('publicacion.html', publicacion=publicacion )
    #return f"Resultados: {publicacion}"

#endregion

#region ESTE ES EL SECTOR PARA CREAR ENTRADAS
@app.errorhandler(DebugFilesKeyError)
def handle_files_key_error(e):
    flash('faltan datos')
    return redirect('/crear')

@app.route('/crear')
def crear():
    return render_template('crear.html')



def extensiones_fotos(nombrefoto):
    extensiones_permitidas = {'jpg', 'jpeg', 'gif', 'webp'}
    return '.' in nombrefoto and nombrefoto.rsplit('.', 1)[1].lower() in extensiones_permitidas


@app.route('/recibe_crear_entrada',  methods=['POST'])
def recibe_crear():
    
    conn = sqlite3.connect('elibre.db')  # Reemplaza 'nombre_base_datos.db' con el nombre de tu base de datos SQLite
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM clave_supersecreta")
    datos_usuario = cursor.fetchall()

    for datos in datos_usuario:
        usu_base = datos[1]
        clave_base = datos[2]
    
    titulo = request.form['titulo'] 
    resumen = request.form['resumen'] 
    texto = request.form['texto'] 
    clave = request.form['clave'] 
    
    fecha=datetime.strftime(datetime.now(), '%d-%m-%Y %H:%M')

    

    autor= usu_base
    
    if clave == clave_base: # Guardar imagen en la base de datos como objeto BLOB 

        # Guardar el archivo en la carpeta "imágenes" del servidor
        if request.files.get('imagen'):
            imagen = request.files.get('imagen')
        
            if not extensiones_fotos(imagen.filename):
                flash('Formato de archivo no admitido. Solo se permiten archivos con extensiones .jpg, .gif o .webp.')
                return redirect(url_for('crear'))

            else:
                # Crear el directorio si no existe
                directorio_imagenes = 'static/imagenes'
                if not os.path.exists(directorio_imagenes):
                    os.makedirs(directorio_imagenes)

                # Guardar el archivo en el directorio
                nombre_archivo = secure_filename(imagen.filename)
                ruta_archivo = os.path.join(directorio_imagenes, nombre_archivo)
                
                imagen.save(ruta_archivo)
                
                _, extension = os.path.splitext(ruta_archivo) #esto es una variable temporal -,
            
                # Verificar si la extensión es .jpg, .jpeg, .JPG o .JPEG
                if extension.lower() in ['.jpg', '.jpeg', '.JPG', '.JPEG']:
                    imagen1 = Image.open(ruta_archivo)
                    imagen1.thumbnail((800,675), Image.ANTIALIAS)
                    imagen1.save(ruta_archivo)
        else:
            nombre_archivo = "sin-imagen.webp"
        
        #Grabar los datos en la base
        cursor.execute('INSERT INTO blog (titulo, resumen, texto, imagen, autor, fecha) VALUES (?, ?, ?, ?, ?, ?)', (titulo, resumen, texto,  nombre_archivo, autor, fecha)) 
        conn.commit()
        cursor.close()
        flash('Datos cargados') 
        return redirect(url_for('crear')) 

    else: # Si la clave es incorrecta, mostrar un mensaje de error al usuario 
        flash('La clave es incorrecta') 
        return redirect('/crear')

#endregion

#region ESTE ES EL SECTOR DE LA PRESENTACIÓN SI QUERÉS CORRER EL SCRIPT
@app.route('/presentacion')
def presentacion ():
    conn = sqlite3.connect('elibre.db')  # Reemplaza 'nombre_base_datos.db' con el nombre de tu base de datos SQLite
    cursor = conn.cursor()

    # Obtener el total de elementos
    cursor.execute("SELECT * FROM presentacion")  # Reemplaza 'tabla' con el nombre de tu tabla
    presentaciones = cursor.fetchall()

    return render_template('presentacion.html', presentaciones=presentaciones)
#endregion


#region ESTE ES EL SECTOR DEL BOTÓN DE PÁNICO
@app.route('/relaja')
def para_mas_placer ():

    # 1) Genero lista para guardar los nombres de las fotos y la ruta
    imagen =[]
    carpeta = "static/imagenes/imagenes_especiales/"
    
        
    # 2)  Recorrer el directorio de imágenes especiales y 
            #Guardar en una lista los nombres de las mismas.
    for archivo in os.listdir(carpeta):
        imagen.append(archivo)

    # 3) Seleccionar una al azar y guardarla en la variable nueva para publicar.
    import random
    imagen_seleccionada = random.choice(imagen)
    relaja = "static/imagenes/imagenes_especiales/"+imagen_seleccionada

    return render_template('relaja.html', relaja=relaja)
#endregion

@app.route('/lista_entradas')
def lista_entradas():
    conexion= sqlite3.connect('elibre.db')
    cursor= conexion.cursor()

    cursor.execute ("""SELECT * FROM blog ORDER BY fecha DESC """)
    listado=cursor.fetchall()

    return render_template('lista_entradas.html', listado=listado)
    

if __name__ == '__main__':
    #app.run(host='0.0.0.0', port=8080, debug=True)
    app.run(host='0.0.0.0', debug=True)
