import sqlite3
from datetime import datetime

con = sqlite3.connect("elibre.db")
cursor=con.cursor()


def carga():
    autor=input('Autor: \n')
    clave=input('Creá clave: \n')
    fecha=datetime.strftime(datetime.now(), '%d-%m-%Y %H:%M')

    con.execute("INSERT INTO clave_supersecreta(autor,clave,fecha) VALUES (?,?,?)",(autor,clave, fecha))

    print ("datos cargados")   
    con.commit()

    ### imprime los datos cargados
    datos2 = cursor.execute(" SELECT * FROM clave_supersecreta WHERE ID = (SELECT MAX(ID) FROM clave_supersecreta);")
    for i in datos2:
        print (i)
    
    print ("¡Clave y autor creados. Que tengas buen día!")
############### Fin de función carga ###################


def crear_tabla():
    cursor.execute( """CREATE TABLE clave_supersecreta
            (id INTEGER PRIMARY KEY AUTOINCREMENT,
            autor VARCHAR (255),
            clave VARCHAR (255),
            fecha DATETIME
            );
    """)
    carga()


# Verifica si la tabla existe
print('Verificamos si la tabla existe en la base:')
verifica = cursor.execute(
  """SELECT name FROM sqlite_master WHERE type='table' AND name='clave_supersecreta';""").fetchall()
 
if verifica == []:
    print('No se encontró la tabla. La creamos...')
    crear_tabla()
else:
    print('¡Tabla blog encontrada! Continuamos.')
    carga()
