import os
from PIL import Image

cwd = os.getcwd()

print (cwd)
for archivo in os.listdir(cwd):
    if archivo.endswith('.jpg') or archivo.endswith('.png'):
        im = Image.open(archivo)
        x, y = im.size
        nombre_archivo = archivo.split(".")[0]
        extension_archivo = archivo.split(".")[1]
        print ("El ancho de la imagen es: "+str(x))
        print ("El alto de la imagen es: "+str(y))
        print ("=========================")
        
        if extension_archivo == "png":
            print ("Es PNG. Convirtiendo a JPG \n\n")
            im = im.convert("RGB")
            im.save(cwd+"/zz-"+nombre_archivo+"-imagen_nueva.jpg","JPEG", quality=30, optimize=True) 

        else:
            im.save(cwd+"/zz-"+nombre_archivo+"-imagen_nueva.jpg",quality=30, optimize=True) 
        


