import sqlite3
from datetime import datetime


#Cambiale el nombre de la base y serás libre...
con = sqlite3.connect("elibre.db")
cursor=con.cursor()


def carga():
    autor=input('Autor: \n')
    texto_presentacion=input('Creá un texto de presentación: \n')
    fecha=datetime.strftime(datetime.now(), '%d-%m-%Y %H:%M')

    con.execute("INSERT INTO presentacion(autor,texto_presentacion, fecha) VALUES (?,?,?)",(autor,texto_presentacion, fecha))

    print ("datos cargados")   
    con.commit()

    ### imprime los datos cargados
    datos2 = cursor.execute(" SELECT * FROM presentacion WHERE ID = (SELECT MAX(ID) FROM presentacion);")
    for i in datos2:
        print (i)
    
    print ("¡Presentación creada!")
############### Fin de función carga ###################


def crear_tabla():
    cursor.execute( """CREATE TABLE presentacion
            (id INTEGER PRIMARY KEY AUTOINCREMENT,
            autor VARCHAR (255),
            texto_presentacion TEXT,
            fecha DATETIME
            );
    """)
    carga()


# Verifica si la tabla existe
print('Verificamos si la tabla existe en la base:')
verifica = cursor.execute(
  """SELECT name FROM sqlite_master WHERE type='table' AND name='presentacion';""").fetchall()
 
if verifica == []:
    print('No se encontró la tabla. La creamos...')
    crear_tabla()
else:
    print('¡Tabla blog encontrada! Continuamos.')
    carga()
